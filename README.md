[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# About

Another template for UI5 projects.

Currently, this project only uses `npm` scripts as task runners. It
might be a good idea to hide any other task runners behind `npm`
scripts as well. This way, all tasks are "in one place" while in the
background we can utilize any system that we desire.

# First-Time Setup

After cloning the project, a simple `npm install` will install the modules.

## Set App Name

To change the default `ui5-basic-template-app-name` app name call
`npm run set-app-name` and you will be prompted for the new name.

## Set Project Namespace

To change the default `sap.ui.demo.basicTemplate` namespace call
`npm run set-namespace` and you will be prompted for the new namespace.

The script doesn't mind whether you enter the namespace with dots
(`ui5.hr.learning`) or slashes (`ui5/hr/learning`). Both of these
types of occurrences will be replaced (with the correct notation).

# Naming Conventions

In order to establish a common understanding and feel for all future
projects we came up with some naming conventions we want to follow:

- folder names should be all lower case
- view and controller files should be in CamelCase, starting with an uppercase letter: `BasicView.controller.js`
- function names should be CamelCase, starting with a lowercase letter: `formatTimeForDisplay(oDate)`
- CSS selectors aka class names should be kebab-case: `my-awesome-red`
- helper scripts should be named in kebab-case: `scripts/set-app-name`
- a function that checks for a condition (and thus presumably returns
  a boolean value) should be prefixed `is`, like e.g. in `isLongEnough(aArray)`

## Variable Name Prefix aka Type Hinting

It is strongly recommended to use hungarian notation (name prefixes indicating
the type) for variables and object field names as indicated below.

| Sample          | Type               |
|-----------------|--------------------|
| **s**Id         | string             |
| **o**Model      | object             |
| **$**DomRef     | jQuery object      |
| **i**Height     | int                |
| **m**Parameters | map / assoc. array |
| **a**Entries    | array              |
| **d**Today      | date               |
| **f**Decimal    | float              |
| **b**Enabled    | boolean            |
| **r**Pattern    | RegExp             |
| **fn**Function  | function           |
| **v**Variant    | variant types      |

## Documentation

JSDoc is preferred, focus in function documentation with input and output types.

# Development Environment

## Prettier

[`Prettier`](https://prettier.io/) will be run on the `pre-commit` hook
in order to lint the staged changes in JavaScript and Less files. The
default Prettier rules will be used, with some overrides defined in
[config file](.prettierrc.js).

To work around Prettier CLI lacking the ability to print detailed error
messages when checking failed, we run it as an
[ESLint](https://eslint.org/) plugin. This is defined in
[.eslintrc.yaml](.eslintrc.yaml) for JavaScript files. For less files we use
[Stylelint](https://stylelint.io/) with the prettier plugin.

You might be able to automatically apply Prettier rules within your
editor by using plugins, see [Visual Studio Code](#Visual-Studio-Code)
or [Webstorm](#Webstorm).

### Visual Studio Code

There is a [Plugin](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
for Prettier for Visual Studio Code. For the auto-formatting option in the
editor to work you need to make sure that there are no other formatters
enabled for the project, e.g. ESLint, otherwise the formatters will interfere
with each other.

### Webstorm

In Webstorm we can use the build in support for ESLint

![](documentation/ESLint/ESLintPreferences.png)

After setting this up we see errors from ESLint in the editor

![](documentation/ESLint/ESLintErrorWebstorm.png)

Sine we use the prettier plugin for ESLint also this errors will show up in the editor.
To Fix this error you can use the QuickFix to fix this one Error or the clean the whole file.

![](documentation/ESLint/ESLintQuickFix.png)

another way is to configure a shortcut for the Fix ESLint Problems

![](documentation/ESLint/ESLintShortcut.png)

## Exact npm Package Versions

Packages already in `package.json` are pinned to exact versions.
New packages will be pinned to their latest exact version upon
installation because [`.npmrc`](.npmrc) defines `save-exact = true`.

This guarantees the consistency of the environment on different machines.

> This also means that packages will _not automatically update_ when
running `npm` and instead _need to be updated manually_ if required.

## Browsersync

During development, the project can be served using
[browsersync](https://browsersync.io/). Simply run `npm run serve` and
look at the app at [localhost:3000](http://localhost:3000).

Config lives in [`.browsersync.js`](/.browsersync.js).

This will proxy all calls to [localhost:8080](http://localhost:8080) there the UI5 Server is running.

> If you don't want to use autoreloads or browsersync in general you can
  simply access the app at [localhost:8080](http://localhost:8080) and thus
  circumvent browsersync completely.

### Proxy

The [server.js](./lib/serve.js) contains the standard ui5 server with the
possibility to configure proxy targets for the oData calls.
Also the middlewares from the defined tasks are registered here.

The proxy target can be set in
`ui5.yaml > server > customMiddleware(proxy) > configuration > proxyTarget`.

Proxy routes will be dynamically created according to the entries in
`manifest.json > sap.app > dataSources`.

### Live Less Compiler

See also [here](#less)

We inject `less.js` into `index.html` together with
[styles/style.less](./webapp/styles/style.less), which is the root
for less file inclusion. The live less.js transpiler then takes care
of loading the required files via less.js `@import`s.

## Git Hooks with Husky

The project uses [husky](https://github.com/typicode/husky) to
work with git hooks. Git hooks will be enabled when installing `husky`
via `npm install`. If all goes as planned, you should see something like this
during the run of `npm install`:

```txt
husky > setting up git hooks
husky > done
```

Hooks are defined in [.huskyrc](.huskyrc).

### commitlint

There is a `commit-msg` hook upon which [`husky`](#Husky) will run a check
for whether the formatting policy for the commit message defined by
[`commitizen`](#Commitizen) has been fulfilled.

The format for the message and options for the type can be found
[here](https://github.com/streamich/git-cz#commit-message-format)

## Commitizen

Contributors are ~~encouraged~~ forced to follow the
[Angular Git Commit Guidelines](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines),
and should use [commitizen](https://github.com/commitizen/cz-cli) when
authoring commit messages.

In order to use `commitizen`'s features, stage your changes as usual.
Then, instead of running `git commit`, use `npm run commit`
to start the interactive prompt on the command line.

GUI users generally will have to author compliant messages by themselves.

However, [here](https://gist.github.com/motin/5896c5b04d039aac48e6a2985d12171b)
is a workaround for SourceTree.

For Webstorm there is a plugin that can create you corresponding commit
messages it can be found [here](https://plugins.jetbrains.com/plugin/9861-git-commit-template)

### Semantic Versioning

This whole setup surrounding the commit messages opens the opportunity to
get [Semantic Versioning](https://semver.org/) for free via
[Semantic Release](https://semantic-release.gitbook.io/semantic-release/).

This is implemented in the feature branch `feature/semantic_release`.

## Editorconfig

There is an [`.editorconfig`](https://editorconfig.org/) [file](.editorconfig)
defining some conventions for editors. Depending on your editor you might have
to enable usage of that file manually. The rules in `.editorconfig` will be
overridden by `prettier`'s config.

## Internet Explorer

For IE we transpile the code to ES5 see [here](#Babel-Custom-Task).

There's also the [babel polyfill](https://babeljs.io/docs/en/babel-polyfill)
included in [`Component.js`](webapp/Component.js), which provides some missing
functionality required for real ES6 in older browsers.

# Building

## UI5 Tooling

The initial building process is done by the
[UI5 command line tools](https://github.com/SAP/ui5-cli).
Run `npm run build` to build the app to `dist/`.

It is possible to "inject" custom sub-tasks into the build task of the
UI5 tooling.

Currently there are the following are defined.

### Replace Namespace on Deploy

This [Task](./lib/tasks/replaceNamespace.js) will change all occurrences of the namespace defined in the `manifest.json` :

```json
{
  "_version": "1.7.0",
  "sap.app": {
    "id": "sap.ui.demo.basicTemplate", <---- This
```

with the configured `deploymentNamespace`

```yaml
specVersion: '0.1'
metadata:
  name: myproject
type: application
builder:
  customTasks:
    - name: replaceNamespace
      afterTask: generateVersionInfo
      configuration:
        deploymentNamespace: "sap.ui.demo.basicTemplate"
```

### Babel Custom Task

#### Building

We have a custom [Babel](https://babeljs.io/) sub-task for the UI5 build task.
It is defined in [ui5.yaml](ui5.yaml) and implemented in
[lib/tasks/transpile.js](./lib/tasks/transpile.js).

#### Development

This task also runs in the local server as a
[middleware](./lib/middleware/transpile.js), meaning you can
develop locally in IE as in any other browser without having to build first.
However, if you develop in a browser that is ES6 compatible this step is
unnecessary and can cost quite some time for every reload, so you might want
to disable that in [ui5.yaml](ui5.yaml) by commenting it out.

The configuration for the babel task can be found in [.babelrc.js](.babelrc.js)

### Less

> TODO: We (potentially) use different less compilers for live dev
  and for deployment, dat bad!!!

#### Building

When building the project with `ui5 build`, `less` files will be compiled using
the [less-openui5](https://github.com/SAP/less-openui5) package as it is
defined in `package.json`.

#### Development

During development, we compile the less files live in the browser.
There is a reference to our base `.less` file containing the rules.
These rules are then compiled by the less compiler, which during live
compiling is taken from _the current UI5 version, not_ the version defined
in `package.json`.

While there is a risk of having different versions, it is almost zero:
SAP UI5 will probably never change the less version because that would lead
to all kinds of complications, e.g. themes becoming incompatible etc.

Therefore, we assume that the version will never change and will always
be the same even in two different packages.

### Remove Dev Only Block

This [task](./lib/tasks/removeDevOnlyBlocks.js) will remove blocks marked like
this in the `dist` folder from `xml` and `html` files:

```html
    <!-- remove:dist -->
    <link rel="stylesheet" type="text/less" href="styles/style.less" />
    <!-- remove:dist -->
```

It is also possible to remove lines from the JavaScript part inside e.g. the
`index.html` like this:

```js
    // <!-- remove:dist -->
    sap.ui.requireSync("sap/ui/thirdparty/less");
    // <!-- remove:dist -->
```

# Deployment

An npm task `deploy` is defined in `package.json`. It uses the
[`nwabap-ui5uploader` tool](https://github.com/nrdev88/nwabap-ui5uploader)
and has its configuration living in [`.nwabaprc`](.nwabaprc).

Run it by calling `npm run deploy` after you updated the config file.

# Resources

- [Why you should use pinned versions](http://c2fo.io/node/npm/javascript/2016/06/03/protecting-your-product-with-npm-save-exact/)
- [Why `npx` is great](https://medium.com/@maybekatz/introducing-npx-an-npm-package-runner-55f7d4bd282b)
- [Example for UI5 Builder using Babel](https://github.com/petermuessig/my-es6-ui5-app/)
- [How to add sub-tasks to the UI5 build task](https://github.com/SAP/ui5-project/blob/master/docs/BuildExtensibility.md)
