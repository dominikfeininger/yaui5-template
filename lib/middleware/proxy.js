const proxy = require("http-proxy-middleware");
const log = require("@ui5/logger").getLogger("custom:proxyMiddleware");

function createMiddleware({resources, options}) {
    let isProxyConfigured = options && options.configuration && options.configuration.proxyTarget;
    if (!isProxyConfigured) {
        return function(req, res, next) {
            next();
        };
    }
    let middlewaresPromise = resources.rootProject
        .byGlob("**/manifest.json")
        .then(resource => resource[0].getString())
        .then(manifestJsonString => JSON.parse(manifestJsonString))
        .then(manifestJson => {
            let dataSources = manifestJson["sap.app"].dataSources;
            let middlewares = [];
            for (let sourceName in dataSources) {
                if (!dataSources.hasOwnProperty(sourceName)) {
                    continue;
                }

                let source = dataSources[sourceName];
                if (source.uri === "/") {
                    log.warn("You registered an OData Service and an Proxy on '/' this will decrease the performance");
                }
                log.info("Add Proxy for " + sourceName + " with url : " + source.uri);
                middlewares.push({
                    uri: source.uri,
                    middleware: proxy({
                        target: options.configuration.proxyTarget,
                        changeOrigin: true,
                        secure: false,
                        followRedirects: true
                    })
                });
            }
            return middlewares;
        });
    return function serveResources(req, res, next) {
        middlewaresPromise.then(middlewares => {
            let bFoundMiddleware = false;
            middlewares.forEach(middlewareObject => {
                if (bFoundMiddleware) {
                    return;
                }
                if (req.url.startsWith(middlewareObject.uri)) {
                    bFoundMiddleware = true;
                    middlewareObject.middleware(req, res, next);
                }
            });
            if (!bFoundMiddleware) {
                next();
            }
        });
    };
}

module.exports = createMiddleware;
