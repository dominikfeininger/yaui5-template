const transpiler = require("../processors/transpile");
const {presets, plugins} = require("../../.babelrc");

function createMiddleware({resources}) {
    return function serveResources(req, res, next) {
        if (req.url.startsWith("/resources") || req.url.startsWith("/test-resources") || !req.url.endsWith(".js")) {
            next();
            return;
        }

        resources.rootProject.byPath(req.url).then(resource => {
            if (!resource) {
                next();
                return;
            }
            return transpiler({resource, plugins, presets}).then(processedResource => {
                res.setHeader("Content-Type", "application/javascript");
                processedResource.getString().then(sContent => {
                    res.end(sContent);
                });
            });
        });
    };
}

module.exports = createMiddleware;
