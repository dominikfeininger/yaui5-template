const {presets, plugins} = require("../../.babelrc");
const transpiler = require("../processors/transpile");

/**
 * Transpiles ES6 code into ES5 code.
 * This is a custom UI5 build task.
 *
 * @param {Object} parameters Parameters
 * @returns {Promise<undefined>} Promise resolving with undefined once data has been written
 */
module.exports = async function({workspace}) {
    let resources = await workspace.byGlob("/**/*.js");
    await Promise.all(
        resources.map(async function(resource) {
            let transpiledResources = await transpiler({resource, presets, plugins});

            return workspace.write(transpiledResources);
        })
    );
};
