const stringReplacer = require("@ui5/builder/lib/processors/stringReplacer");

module.exports = function({workspace}) {
    return workspace
        .byGlobSource("**/*.{html,xml}")
        .then(resources => {
            return stringReplacer({
                resources,
                options: {
                    pattern: /^\s*(\/\/\s?)?<!-- remove:dist -->(.|\n)*?<!-- remove:dist -->\n/gm,
                    replacement: ""
                }
            });
        })
        .then(processedResources => {
            return Promise.all(
                processedResources.map(resource => {
                    return workspace.write(resource);
                })
            );
        });
};
