const compileLess = require("../processors/compileLess");
const ReaderCollectionPrioritized = require("@ui5/fs").ReaderCollectionPrioritized;
const DuplexCollection = require("@ui5/fs").DuplexCollection;
const Memory = require("@ui5/fs").adapters.Memory;
const FileSystem = require("@ui5/fs").adapters.FileSystem;
const fsInterface = require("@ui5/fs").fsInterface;

/**
 * Custom task to compile less in an application
 *
 * @param {Object} parameters Parameters
 * @param {module:@ui5/fs.DuplexCollection} parameters.workspace DuplexCollection to read and write files
 * @param {module:@ui5/fs.AbstractReader} parameters.dependencies Reader or Collection to read dependency files
 * @param {Object} parameters.options Options
 * @param {string} parameters.options.projectName Project name
 * @param {string} [parameters.options.configuration] Task configuration if given in ui5.yaml
 * @returns {Promise<undefined>} Promise resolving with <code>undefined</code> once data has been written
 */
module.exports = async function({workspace, dependencies, options}) {
    let customWorkspace = new DuplexCollection({
        reader: new FileSystem({
            virBasePath: "/",
            fsBasePath: "webapp"
        }),
        writer: new Memory({
            virBasePath: "/"
        })
    });

    const customCombo = new ReaderCollectionPrioritized({
        name: `theme - prioritize workspace over dependencies: ${options.projectName}`,
        readers: [customWorkspace, dependencies]
    });

    let lessResourcesNormalWorkSpace = await workspace.byGlobSource("**/*.less");
    let lessResourcesCustomWorkSpace = await customWorkspace.byGlobSource("**/*.less");

    let lessToCompile = options.configuration.lessToCompile;
    if (typeof lessToCompile === "string") {
        lessToCompile = [lessToCompile];
    }

    lessResourcesCustomWorkSpace = lessResourcesCustomWorkSpace.filter(resource =>
        lessToCompile.some(lessToCompile => resource.getPath().includes(lessToCompile))
    );

    lessResourcesCustomWorkSpace = lessResourcesCustomWorkSpace.map(resource => {
        let resourceInNormalWorspace = lessResourcesNormalWorkSpace.find(a => a.getPath().includes(resource.getPath()));
        return {
            normalPath: resourceInNormalWorspace.getPath(),
            resource: resource
        };
    });

    let processedResources = await compileLess({
        resourcesWithNormalPath: lessResourcesCustomWorkSpace,
        fs: fsInterface(customCombo)
    });

    processedResources = processedResources.reduce((acc, val) => acc.concat(val));

    return Promise.all(
        processedResources.map(resource => {
            return workspace.write(resource);
        })
    );
};
