const babel = require("@babel/core");
module.exports = function({resource, presets, plugins}) {
    return resource.getString().then(function(code) {
        let sourceFileName = resource.getPath().replace("/resources/" + resource._project.metadata.namespace + "/", "");
        return babel
            .transformAsync(code, {
                sourceFileName: sourceFileName,
                sourceMap: "inline",
                presets: presets,
                plugins: plugins
            })
            .then(value => {
                resource.setString(value.code);
                return resource;
            });
    });
};
