function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

module.exports = function({resources, options}) {
    return Promise.all(
        resources.map(resource => {
            return resource.getString().then(function(code) {
                let regModuleNamePlaceholder = new RegExp(escapeRegExp(options.nameSpaceToReplace), "g"),
                    regModuleNamePathPlaceholder = new RegExp(
                        escapeRegExp(options.nameSpaceToReplace.replace(/\./g, "/")),
                        "g"
                    );

                code = code.replace(regModuleNamePlaceholder, options.newNameSpace);
                code = code.replace(regModuleNamePathPlaceholder, options.newNameSpace.replace(/\./g, "/"));

                resource.setString(code);
                return resource;
            });
        })
    );
};
