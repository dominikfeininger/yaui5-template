sap.ui.define(
    [
        "sap/ui/demo/basicTemplate/controller/BaseController", //
        "sap/ui/demo/basicTemplate/model/Formatter"
    ],
    function(
        BaseController, //
        Formatter
    ) {
        "use strict";

        return BaseController.extend("sap.ui.demo.basicTemplate.controller.Overview", {
            formatter: Formatter,

            onInit: function() {
                BaseController.prototype.onInit.apply(this, arguments);
            }
        });
    }
);
