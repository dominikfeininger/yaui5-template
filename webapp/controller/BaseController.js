sap.ui.define(
    [
        "sap/ui/core/mvc/Controller", //
        "sap/ui/core/routing/History"
    ],
    function(Controller, History) {
        "use strict";

        return Controller.extend("sap.ui.demo.basicTemplate.controller.BaseController", {
            /**
             * Get this controller's component's router.
             *
             * @returns {sap.ui.core.routing.Router} this controller's component's router
             */
            getRouter: function() {
                return this.getOwnerComponent().getRouter();
            },

            /**
             * Get this controller's view's model (by name).
             *
             * @param {string} [sName] the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel: function(sName) {
                return this.getView().getModel(sName);
            },

            /**
             * Set a (named) model on this controller's view.
             *
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} [sName] the model name
             * @returns {sap.ui.base.ManagedObject} _this_ to allow method chaining
             */
            setModel: function(oModel, sName) {
                this.getView().setModel(oModel, sName);
                // setModel() would return the view, but since we are in the
                // controller, we'd rather have 'this' returned instead.
                return this;
            },

            /**
             * Get this controller's component's i18n resource bundle.
             *
             * @returns {sap.base.i18n.ResourceBundle} the i18n resourceBundle of the component
             */
            geti18nResourceBundle: function() {
                return this.getOwnerComponent()
                    .getModel("i18n")
                    .getResourceBundle();
            },

            /**
             * Navigates back to previous view
             *
             * @private
             */
            onNavBack: function() {
                var oHistory = History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();

                if (sPreviousHash !== undefined) {
                    // The history contains a
                    // previous entry
                    history.go(-1);
                } else {
                    // Otherwise we go backwards
                    // with a forward history
                    var bReplace = true;
                    this.getRouter().navTo("Overview", {}, bReplace);
                }
            },

            /* =========================================================== */
            /* Event handlers                                              */
            /* =========================================================== */

            onInit: function() {},

            onAfterRendering: function() {}
        });
    }
);
