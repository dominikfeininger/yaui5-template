sap.ui.define(
    [
        "sap/ui/core/UIComponent", //
        "sap/ui/Device",
        "sap/ui/demo/basicTemplate/model/Models",
        "sap/ui/demo/basicTemplate/controller/ErrorHandler",
        "sap/ui/demo/basicTemplate/util/Router", // load here, is missing otherwise
        "sap/ui/demo/basicTemplate/lib/babel/polyfill.min"
    ],
    function(
        UIComponent, //
        Device,
        Models,
        ErrorHandler,
        Router
    ) {
        "use strict";

        return UIComponent.extend("sap.ui.demo.basicTemplate.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the
             * startup of the app and calls the init method once.
             *
             * @public
             * @override
             */
            init: function() {
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // initialize the error handler with the component
                this._oErrorHandler = new ErrorHandler(this);

                // set the device model
                this.setModel(Models.createDeviceModel(), "device");

                this.getRouter().initialize();

                //encapsulate component styling
                $("body").attr("ui-theme", "sap.ui.demo.basicTemplate");
            },

            /**
             * The component is destroyed by UI5 automatically.
             * In this method, the GitHandler are destroyed.
             * @public
             * @override
             */
            destroy: function() {
                // destroy all created Objects
                this._oErrorHandler.destroy();

                UIComponent.prototype.destroy.apply(this, arguments);
            },

            /**
             * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
             * design mode class should be set, which influences the size appearance of some controls.
             * @public
             * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
             */
            getContentDensityClass: function() {
                if (this._sContentDensityClass === undefined) {
                    // check whether FLP has already set the content density class; do nothing in this case
                    if (
                        jQuery(document.body).hasClass("sapUiSizeCozy") ||
                        jQuery(document.body).hasClass("sapUiSizeCompact")
                    ) {
                        this._sContentDensityClass =
                            jQuery(document.body).hasClass("sapUiSizeCozy") ||
                            jQuery(document.body).hasClass("sapUiSizeCompact");
                    } else if (!Device.support.touch) {
                        // apply "compact" mode if touch is not supported
                        this._sContentDensityClass = "sapUiSizeCompact";
                    } else {
                        // "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
                        this._sContentDensityClass = "sapUiSizeCozy";
                    }
                }
                return this._sContentDensityClass;
            }
        });
    }
);
