sap.ui.define(["sap/ui/test/opaQunit", "sap/ui/test/Opa", "./pages/App"], function(OpaQunit, Opa) {
    "use strict";

    QUnit.module("Navigation Journey");

    opaTest("Should see the initial page of the app", function(Given, When, Then) {
        // Arrangements
        Given.iStartMyUIComponent({
            componentConfig: {
                name: "sap.ui.demo.basicTemplate",
                manifestFirst: true,
                async: true
            }
        });

        // Assertions
        Then.onTheAppPage.iShouldSeeTheApp();

        //Cleanup
        Then.iTeardownMyAppFrame();
    });
});
