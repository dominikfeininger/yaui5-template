sap.ui.define(["sap/ui/test/Opa5", "./navigationJourney"], function(Opa5) {
    "use strict";

    Opa5.extendConfig({
        viewNamespace: "sap.ui.demo.basicTemplate.view.",
        autoWait: true
    });
});
