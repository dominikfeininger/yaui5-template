// these are only overrides of the standards, for a full list
// and descriptions see https://prettier.io/docs/en/options.html

module.exports = {
    "bracketSpacing": false,
    "printWidth": 120,
    // "trailingComma": "all",  // we should use that some day
};
