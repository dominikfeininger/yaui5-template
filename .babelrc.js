// https://babeljs.io/docs/en/config-files#project-wide-configuration

const presets = [
    [
        "@babel/env",

        {
            targets: {
                // https://github.com/browserslist/browserslist
                // for more info on targets
                ie: "11",
                edge: "17",
                firefox: "60",
                chrome: "67",
                safari: "11.1"
            }
        }
    ]
];
const plugins = [];

module.exports = {
    presets,
    plugins
};
